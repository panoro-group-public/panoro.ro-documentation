const swaggerJsdoc = require('swagger-jsdoc');
const path = require('path');
const jsonfile = require('jsonfile');

const AppPanoroServerDir = path.join(__dirname, '..', '..', 'packages', 'nodejs_app_panoro_server');

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Hello World',
            version: '1.0.0',
        },
    },
    apis: [path.join(AppPanoroServerDir, 'src', 'modules', 'estates', 'api', '*')],
};

async function run() {
    const openapiSpecification = swaggerJsdoc(options);
    console.log(openapiSpecification);
    jsonfile.writeFileSync('spec.json', openapiSpecification);
    process.exit(0);
}

run();
